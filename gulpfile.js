// Gulp dependencies
var gulp = require('gulp'),
  buffer = require('vinyl-buffer'),
    merge = require('merge-stream'),
    spritesmith = require('gulp.spritesmith'),
    connect = require('gulp-connect');

var HTML_DIR = '*.html',
    CSS_DIR = 'dist/css/*.css',
    JS_DIR = 'dist/js/*.js',
    SPRITES_DIR = 'dist/_sprites/*.png';

// Tasks
gulp.task('connect', function() {
  connect.server({
    port: 9001,
    livereload: true
  })
});

gulp.task('html', function() {
  return gulp.src(HTML_DIR)
    .pipe(connect.reload());
});

gulp.task('css', function() {
  return gulp.src(CSS_DIR)
    .pipe(connect.reload());
});

gulp.task('js', function() {
  return gulp.src(JS_DIR)
    .pipe(connect.reload());
});

gulp.task('spritesmith', function() {
  var data = gulp.src(SPRITES_DIR)
    .pipe(spritesmith({
      padding: 5,
      imgName: 'sprites.png',
      imgPath: '../img/sprites.png',
      cssName: 'sprites.css'
    }));

  var imgStream = data.img
    .pipe(buffer())
    .pipe(gulp.dest('dist/img'));

  var cssStream = data.css
    .pipe(buffer())
    .pipe(gulp.dest('dist/css'));

  return merge(imgStream, cssStream);
});

// Default task
gulp.task('default', ['connect'], function() {
  gulp.watch(HTML_DIR, ['html']);
  gulp.watch(CSS_DIR, ['css']);
  gulp.watch(JS_DIR, ['js']);
  gulp.watch(SPRITES_DIR, ['spritesmith']);
});