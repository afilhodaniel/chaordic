function X(json) {
  recommendation().populateReference(json.data.reference.item);
  recommendation().populateItems(json.data.recommendation);
  recommendation().carousel(json.data.widget.size).bootstrap();
}

function ajax(options) {
  var result = {
    success: false
  };

  var xmlHttpRequest = new XMLHttpRequest();

  xmlHttpRequest.open(options.method, options.url, false);

  xmlHttpRequest.onload = function() {
    if(xmlHttpRequest.status == 200) {
      result = {
        success: true,
        data: xmlHttpRequest.responseText
      }
    }
  };

  xmlHttpRequest.onloadend = function() {
    setTimeout(function() {
      document.getElementsByClassName('showcase-loading')[0].classList.remove('active');
    }, 1000);
  };

  xmlHttpRequest.send();

  return result;
}

function loadTemplate(method, name) {
  var url = 'dist/templates/' + name;

  return ajax({
    method: method,
    url: url
  });
}

function recommendation() {
  return {
    parseTemplate: function(data, template) {
      var html = '';
      var paymentConditions = '';
      var protocol = window.location.protocol;

      html = template.replace(':recommendationLink', protocol + data.detailUrl)
        .replace(':recommendationTitle', data.name)
        .replace(':recommendationTitle', data.name)
        .replace(':recommendationTitle', data.name.slice(0, 60) + '...')
        .replace(':recommendationThumb', protocol + data.imageName)
        .replace(':recommendationPrice', data.price)
        .replace(':recommendationPaymentConditions', data.productInfo.paymentConditions.replace(new RegExp('[0-9]{2}.[0-9]{2}'), function(value) {
          return 'R$ ' + value.replace('.', ',');
        }));

      if(data.oldPrice != null) {
        html = html.replace(':recommendationOldPrice', data.oldPrice);
      } else {
        html = html.replace('<span class="recommendation-item-old_price">De: :recommendationOldPrice</span>', '');
      }

      return html;
    },
    populateReference: function(data) {
      template = loadTemplate('GET', 'recommendation-item.html');

      if(template.success) {
        document.getElementsByClassName('showcase-reference-content')[0].innerHTML = this.parseTemplate(data, template.data);
      }
    },
    populateItems: function(data) {
      var _this = this;
      var template = loadTemplate('GET', 'recommendation-list-item.html');

      if(template.success) {
        var html = '';

        data.forEach(function(elem) {
          html += _this.parseTemplate(elem, template.data);
        });

        document.getElementsByClassName('showcase-recommendations-list')[0].innerHTML = html;
      }
    },
    carousel: function(size) {
      var totalSlides = size;
      var currentSlide = 4;
      var totalMarginLeft = 0;

      return {
        bootstrap: function() {
          var totalWidth = 0;
          var items = document.getElementsByClassName('showcase-recommendations-list-item');

          for(var i = 0, length = items.length; i < length; i++) {
            totalWidth += items[i].clientWidth;
          }

          document.getElementsByClassName('showcase-recommendations-list')[0].style.width = totalWidth + 'px';

          this.next();
          this.prev();
        },
        next: function() {
          document.getElementsByClassName('next')[0].addEventListener('click', function() {
            if(currentSlide < 10) {
              totalMarginLeft += -190;
              currentSlide++;
            }

            document.getElementsByClassName('showcase-recommendations-list')[0].style.marginLeft = totalMarginLeft + 'px';
          });
        },
        prev: function() {
          document.getElementsByClassName('prev')[0].addEventListener('click', function() {
            if(currentSlide > 4) {
              totalMarginLeft += 190;
              currentSlide--;
            }

            document.getElementsByClassName('showcase-recommendations-list')[0].style.marginLeft = totalMarginLeft + 'px';
          });
        }
      };
    },
  };
}